# mpv_jump_to_time
MPV script that jumps to a prompted time in the video

-- Important: You must have a keybinding in order to use this functionality
-- Important: Put this file as a .lua into the (*nix: ~/.config/mpv/scripts/) (Windows: %AppData%\Roaming\mpv\scripts\) (MacOS: *I do not know where this folder is, let me "Jesse E Coyle" know*) folder


-- Note: To set a keybinding through the input.conf, edit the input.conf file in the (*nix: ~/.config/mpv/) (Windows: %AppData%\Roaming\mpv\) folder.
--       If there is no file there, make one
--       Next, put "g script-binding JumpToPrompt" on a new line, where 'g' is the key you press to start the script, change how you'd like
